## A simple twilio SMS REST sample implemented in Python

# Requirements:
1. Python 2.7+
2. pip installed
3. flask, twilio modules are installed

# To Use:
1. Go to the "twilio-sms-python" directory
2. python run_app.py
3. From terminal, do a ```curl -H "Content-Type: application/json" -X POST -d '{"cellphone_number":"xxxxxxx"}' http://127.0.0.1:5000/send_code```
4. You should receive a SMS message! (North America Only)
