from __future__ import (absolute_import, print_function)
"""This script sends verfication code to a given cellphone number"""
from flask import request
from app import app
import twilio.twiml
from twilio.rest import TwilioRestClient

# Setup auth tokens
# Repalce the xxxxxx with your own twilio sid and token
account_sid = "xxxxxxxxxxxxxxxx"
auth_token = "xxxxxxxxxxxxxxxxx"
client = TwilioRestClient(account_sid, auth_token)

@app.route('/send_code', methods=['POST'])
def send_code():
    # Get posted data
    data = request.get_json(force=True)
    # Remove "-" from cellphone number
    cellphone_number = data['cellphone_number']
    cellphone_number = ''.join([number for number in cellphone_number if number not in '-'])
    # Hard code country code for now
    country_code = '+1'
    # Send verification code
    msg = client.messages.create(
        # You need to update the from_ to the number you registered in twilio
        to="+1{0}".format(cellphone_number), from_="+1xxxxxxxxxxx",
        body= 'Congurats! SMS worked!')
    return 'Message has been sent!'
